USE [Sorteo]
GO
/****** Object:  Table [dbo].[Configuracion]    Script Date: 27/11/2022 11:19:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Configuracion](
	[id] [int] NULL,
	[Evento] [varchar](80) NULL,
	[CantidadGanadores] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Num_Libres]    Script Date: 27/11/2022 11:19:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Num_Libres](
	[Num_Libre] [int] NULL,
	[Estado] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Participantes]    Script Date: 27/11/2022 11:19:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Participantes](
	[id] [int] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Apellido] [varchar](50) NULL,
	[Telefono] [varchar](10) NULL,
	[Num_Escogido] [int] NOT NULL,
	[estado] [varchar](10) NULL,
 CONSTRAINT [pk_NumEscogido] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Num_Libres] ADD  CONSTRAINT [DF_Num_Libres_Estado]  DEFAULT ('Activo') FOR [Estado]
GO
ALTER TABLE [dbo].[Participantes] ADD  CONSTRAINT [DF_Participantes_estado]  DEFAULT ('Activo') FOR [estado]
GO
/****** Object:  StoredProcedure [dbo].[SP_Actualizar]    Script Date: 27/11/2022 11:19:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_Actualizar](
@Nombre varchar(50),
@Apellido varchar(50),
@Telefono varchar(50),
@Num_Escogido int,
@id int
)

AS
BEGIN

UPDATE Participantes SET Nombre = @Nombre, Apellido = @Apellido, Telefono = @Telefono, Num_Escogido = @Num_Escogido WHERE id = @id

END
GO
/****** Object:  StoredProcedure [dbo].[SP_Eliminar]    Script Date: 27/11/2022 11:19:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Eliminar](
@id int
)
AS
BEGIN

DELETE FROM Participantes WHERE id = @id

END
GO
/****** Object:  StoredProcedure [dbo].[SP_IngresarParticipantes]    Script Date: 27/11/2022 11:19:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_IngresarParticipantes](
@Nombre varchar(50),
@Apellido varchar(50),
@Telefono varchar(10),
@Num_Escogido varchar(10),
@id int
)
AS
BEGIN

INSERT INTO Participantes (Nombre, Apellido,Telefono, Num_Escogido, id) VALUES (@Nombre,@Apellido,@Telefono,@Num_Escogido,@id)

END
GO
/****** Object:  StoredProcedure [dbo].[SP_VerTodoParticipantes]    Script Date: 27/11/2022 11:19:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_VerTodoParticipantes]

AS
BEGIN
	SELECT*FROM Participantes ORDER BY Num_Escogido ASC
END
GO
