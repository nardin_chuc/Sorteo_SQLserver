
package Controlador;

import Sorteo.Conexion;
import Sorteo.Participantes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Numeros {
    
    Conexion cn = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    
    public Participantes encontrarNumero(int NumeroEncontrado){
        String sql = "SELECT * FROM Participantes WHERE  Num_Escogido = ? ";
        Participantes par = new Participantes();
        try {
            con = cn.establecerConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, NumeroEncontrado);
            rs = ps.executeQuery();
            if (rs.next()) {
                par.setNombre(rs.getString("Nombre"));
                par.setNumEscogido(""+rs.getInt("Num_Escogido"));
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
        return par;
    }
}
