/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Hilos.HiloNum;
import Hilos.Tables;
import Sorteo.Conexion;
import Sorteo.Eleccion;
import Sorteo.Ganador;
import Sorteo.Participantes;
import java.awt.Color;
import static java.awt.Color.BLACK;
import static java.awt.Color.BLUE;
import static java.awt.Color.RED;
import static java.awt.Color.WHITE;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author CCNAR
 */
public class ControladorEleccion implements ActionListener, MouseListener {

    int contador = 1, ganador = 0, NumeroEncontrado = 0;
    int ganador1 = 0, ganador2 = 0, ganador3 = 0, ganador4 = 0;
    int NumMaximo;
    int cantidad = 0;
    int[] numeros, numAux, arreglo;
    JTextField[] numRecibidos;
    int aux;
    HiloNum Hnum;

    Eleccion elec;
    Numeros num = new Numeros();
    Participantes par = new Participantes();
    DefaultTableModel modelo = new DefaultTableModel();

    public ControladorEleccion(Eleccion elec, Numeros num, Participantes par) {
        this.elec = elec;
        this.num = num;
        this.par = par;
       // cargarTabla();
        CargarParticipantes("");
        CantidadMaxTabla();
        NumMaximo = Integer.parseInt(elec.txt_numMax.getText());
        cantidad = Integer.parseInt(elec.txt_cant.getText()) + 1;
        ejecutarGanador();
        inicio(false);
        elec.btn_OtroNum.addActionListener(this);
        elec.btn_Empezar.addActionListener(this);
        elec.btn_VerLista.addActionListener(this);
        elec.btn_Modificar.addActionListener(this);
        elec.tb_Participantes.addMouseListener(this);
        elec.tb_ganadores.addMouseListener(this);
        elec.txt_NumElegido.setVisible(false);
        MeterNum();
        VerPaneles(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == elec.btn_Empezar) {
            inicio(true);
            elec.btn_Empezar.setVisible(false);
        } else if (e.getSource() == elec.btn_OtroNum) {
            VerPaneles(true);
            if ((contador) < cantidad) {
                meterNumero(contador);
                contador++;
                if (Verificar(NumeroEncontrado) == true && !(NumeroEncontrado == ganador2 || NumeroEncontrado == ganador1 || NumeroEncontrado == ganador3 || NumeroEncontrado == ganador4)) {
                    try {
                        String estado = "Inactivo";
                        Connection conectar = Conexion.establecerConnection();
                        PreparedStatement ps = conectar.prepareStatement("UPDATE Participantes SET estado = ? WHERE Num_Escogido = ? ");
                        ps.setString(1, estado);
                        ps.setString(2, String.valueOf(NumeroEncontrado));
                        ps.executeUpdate();
                        //cargarTabla();
                        limpiarTable();
                        CargarParticipantes(elec.txt_NumElegido.getText());
                        JOptionPane.showMessageDialog(null, "Participante Eliminado del Sorteo");
                    } catch (Exception er) {
                    }
                } else if (Verificar(NumeroEncontrado) == true || NumeroEncontrado == ganador2 || NumeroEncontrado == ganador1 || NumeroEncontrado == ganador3 || NumeroEncontrado == ganador4) {
                    try {
                        ganador++;
                        String estado = "Ganador " + ganador;
                        Connection conectar = Conexion.establecerConnection();
                        PreparedStatement ps = conectar.prepareStatement("UPDATE Participantes SET estado = ? WHERE Num_Escogido = ? ");
                        ps.setString(1, estado);
                        ps.setString(2, String.valueOf(NumeroEncontrado));
                        ps.executeUpdate();
                        //cargarTabla();
                        limpiarTable();
                        CargarParticipantes(elec.txt_NumElegido.getText());
                        Ganador Ganador = new Ganador();
                        Ganador.setVisible(true);
                        Ganador.MostrarGanador(ganador);
                        elec.tb_ganadores.setVisible(true);
                        cargarTablaGanador("Ganador"); 
                       // JOptionPane.showMessageDialog(null, "Ganador " + ganador + " del Sorteo");
                    } catch (Exception er) {
                    }
                } else if (Verificar(NumeroEncontrado) == false) {
                    JOptionPane.showMessageDialog(null, "No se Encontro Participante");
                }
            }else{
                elec.btn_OtroNum.setEnabled(false);
                elec.jButton1.setFocusable(true);
            }

        } else if (e.getSource() == elec.btn_Modificar) {
            int res = JOptionPane.showConfirmDialog(null, "Reiniciar sorteo\n " + "¿ Desea seguir ?");
            String estado = "Activo";
            
            if (res == 0) {
                inicio(true);
                ejecutarGanador();
                try {
                    Connection conectar = Conexion.establecerConnection();
                    String sql = "UPDATE Participantes SET estado = ?";
                    PreparedStatement ps = conectar.prepareStatement(sql);
                    ps.setString(1, estado);
                    ps.executeUpdate();
                    elec.txt_NumElegido.setText("");
                    Limpiar(arreglo, numAux);
                    VerPaneles(false);
                    limpiarTable();
                    CargarParticipantes(elec.txt_NumElegido.getText());
                    cargarTablaGanador("");
                    JOptionPane.showMessageDialog(null, "Sorteo Reiniciado");
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorEleccion.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                elec.btn_OtroNum.requestFocus(true);
            }
        }else if(e.getSource() == elec.btn_VerLista){
            limpiarTable();
            CargarParticipantes("");
        }

    }

    public void buscarNumero(int numero) {
        for (int i = 0; i < contador; i++) {
            if (numAux[i] == numero) {
                meterNumero(contador);
            } else {

            }
        }
    }

    public void meterNumero(int contador) {
        aux = (int) (Math.random() * cantidad + 1);
        for (int i = contador; i < contador + 1; i++) {
            numAux[i] = aux;
            buscarNumero(aux);
        }
        if (aux < cantidad) {
            NumeroEncontrado = numeros[aux];
            arreglo[contador] = numeros[aux];
            imprimirNumeros(arreglo, contador);
            elec.lb_restantes.setText("Participantes: " + (contador) + " / "+elec.txt_cant.getText());
            elec.lb_Numero.setText("" + NumeroEncontrado);
            elec.txt_NumElegido.setText("" + NumeroEncontrado);
        } else {
            meterNumero(contador);
        }
    }
    
    public int cantGanadorSQL(){
        PreparedStatement ps;
        ResultSet rs;
        String sql = "SELECT CantidadGanadores FROM Configuracion ";
        int cantGnada = 0;
        try {
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {                
                cantGnada = rs.getInt("CantidadGanadores");
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("error en buscar cant ganador sql "+e.getMessage());
        }
        return cantGnada;
    }
    
    public void ejecutarGanador(){
        int cantGanador = cantGanadorSQL();
        if(cantGanador == 1){
            ganador1 = BuscarGanador(1);
        }else if(cantGanador == 2){
            ganador1 = BuscarGanador(1);
            ganador2 = BuscarGanador(2);
        }else if(cantGanador == 3){
            ganador1 = BuscarGanador(1);
            ganador2 = BuscarGanador(2);
            ganador3 = BuscarGanador(3);
        }else if(cantGanador == 4){
            ganador1 = BuscarGanador(1);
            ganador2 = BuscarGanador(2);
            ganador3 = BuscarGanador(3);
            ganador4 = BuscarGanador(4);
        }
    }
    
    public int BuscarGanador(int suma) {
        int numAux =  (int) ((Math.random()) * (cantidad+suma) );
        if(numAux == ganador1 || numAux == ganador2 || numAux == ganador3 || numAux == ganador4){
            BuscarGanador(suma*2);
        } else{
            return numAux;
        } 
        return numAux;
    }

    public void MeterNum() {
        PreparedStatement ps;
        ResultSet rs;
        String sql = "SELECT Num_Escogido FROM Participantes ";
        try {
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            int i = 1;
            numeros = new int[cantidad];
            numAux = new int[cantidad];
            numRecibidos = new JTextField[cantidad];
            arreglo = new int[cantidad];
            while (rs.next()) {
                numeros[i] = rs.getInt("Num_Escogido");
                i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControladorEleccion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public void cargarTabla() {
        DefaultTableModel modeloTabla = (DefaultTableModel) elec.tb_Participantes.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 300, 150, 200, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            elec.tb_Participantes.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            String sql = "SELECT Nombre, Apellido, Telefono, Num_Escogido, estado FROM Participantes ORDER BY Num_Escogido ASC,  estado ASC";

            Connection con = Conexion.establecerConnection();

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }
    
    
    

    public boolean Verificar(int numEncontrado) {
        par = num.encontrarNumero(numEncontrado);
        if (par.getNumEscogido() != null) {
           // JOptionPane.showMessageDialog(null, "Se repite el numero escogido");
            limpiarTable();
            return true;
        } else {
            limpiarTable();
            CargarParticipantes("");
            return false;
        }

    }

    public void imprimirNumeros(int numeros[], int contador) {
        if (contador <= 19) {
            elec.jPanel_num_Aparecidos.removeAll();
            for (int i = 1; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                elec.jPanel_num_Aparecidos.add(numRecibidos[i]);
                elec.pack();
                if (this.numeros[aux] == numeros[i]) {
                    numRecibidos[i].setBackground(BLUE);
                    numRecibidos[i].setForeground(WHITE);
                    elec.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    elec.pack();
                }
            }
        } else if (contador >= 20 && contador <= 39) {
            elec.jPanel_num_Aparecidos1.removeAll();
            for (int i = 20; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                elec.jPanel_num_Aparecidos1.add(numRecibidos[i]);
                elec.pack();
                if (this.numeros[aux] == numeros[i]) {
                    numRecibidos[i].setBackground(RED);
                    numRecibidos[i].setForeground(WHITE);
                    elec.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    elec.pack();
                }
            }
        } else if (contador >= 40 && contador <= 59) {
            elec.jPanel_num_Aparecidos2.removeAll();
            for (int i = 40; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                elec.jPanel_num_Aparecidos2.add(numRecibidos[i]);
                elec.pack();
                if (this.numeros[aux] == numeros[i]) {
                    numRecibidos[i].setBackground(BLUE);
                    numRecibidos[i].setForeground(WHITE);
                    elec.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    elec.pack();
                }
            }
        } else if (contador >= 60 && contador <= 79) {
            elec.jPanel_num_Aparecidos3.removeAll();
            for (int i = 60; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                elec.jPanel_num_Aparecidos3.add(numRecibidos[i]);
                elec.pack();
                if (this.numeros[aux] == numeros[i]) {
                    numRecibidos[i].setBackground(RED);
                    numRecibidos[i].setForeground(WHITE);
                    elec.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    elec.pack();
                }
            }
        } else if (contador >= 80 && contador <= 99) {
            elec.jPanel_num_Aparecidos4.removeAll();
            for (int i = 80; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                elec.jPanel_num_Aparecidos4.add(numRecibidos[i]);
                elec.pack();
                if (this.numeros[aux] == numeros[i]) {
                    numRecibidos[i].setBackground(BLUE);
                    numRecibidos[i].setForeground(WHITE);
                    elec.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    elec.pack();
                }
            }
        } else if (contador >= 100 && contador <= 119) {
            elec.jPanel_num_Aparecidos5.removeAll();
            for (int i = 80; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                elec.jPanel_num_Aparecidos5.add(numRecibidos[i]);
                elec.pack();
                if (this.numeros[aux] == numeros[i]) {
                    numRecibidos[i].setBackground(BLUE);
                    numRecibidos[i].setForeground(WHITE);
                    elec.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    elec.pack();
                }
            }
        } else if (contador >= 120 && contador <= 139) {
            elec.jPanel_num_Aparecidos6.removeAll();
            for (int i = 80; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                elec.jPanel_num_Aparecidos6.add(numRecibidos[i]);
                elec.pack();
                if (this.numeros[aux] == numeros[i]) {
                    numRecibidos[i].setBackground(BLUE);
                    numRecibidos[i].setForeground(WHITE);
                    elec.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    elec.pack();
                }
            }
        } else if (contador >= 140 && contador <= 149) {
            elec.jPanel_num_Aparecidos6.removeAll();
            for (int i = 80; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                elec.jPanel_num_Aparecidos6.add(numRecibidos[i]);
                elec.pack();
                if (this.numeros[aux] == numeros[i]) {
                    numRecibidos[i].setBackground(BLUE);
                    numRecibidos[i].setForeground(WHITE);
                    elec.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    elec.pack();
                }
            }
        }

    }

    public void VerPaneles(boolean ver) {

        elec.jPanel_num_Aparecidos.setVisible(ver);
        elec.jPanel_num_Aparecidos1.setVisible(ver);
        elec.jPanel_num_Aparecidos2.setVisible(ver);
        elec.jPanel_num_Aparecidos3.setVisible(ver);
        elec.jPanel_num_Aparecidos4.setVisible(ver);
        elec.jPanel_num_Aparecidos5.setVisible(ver);
        elec.jPanel_num_Aparecidos6.setVisible(ver);
        elec.jPanel_num_Aparecidos7.setVisible(ver);
    }

    public void Limpiar(int numero[], int auxNum[]) {
        elec.jPanel_num_Aparecidos.removeAll();
        elec.jPanel_num_Aparecidos1.removeAll();
        elec.jPanel_num_Aparecidos2.removeAll();
        elec.jPanel_num_Aparecidos3.removeAll();
        elec.jPanel_num_Aparecidos4.removeAll();
        elec.jPanel_num_Aparecidos5.removeAll();
        elec.jPanel_num_Aparecidos6.removeAll();
        elec.jPanel_num_Aparecidos7.removeAll();
        elec.lb_Numero.setText("");
        VerPaneles(false);
        contador = 1;
        elec.lb_restantes.setText("");
        for (int i = 0; i < numero.length; i++) {
            numero[i] = 0;
            auxNum[i] = 0;
        }

    }

    public void inicio(boolean vista) {
        elec.btn_OtroNum.setVisible(vista);
        elec.btn_OtroNum.setEnabled(vista);
        elec.btn_Modificar.setVisible(vista);
        elec.btn_VerLista.setVisible(vista);
    }

    public List listarParticipantes(String Num) {
        PreparedStatement ps;
        ResultSet rs;
        List<Participantes> listaParticipantes = new ArrayList();
        String sql = "SELECT Nombre, Apellido, Telefono, Num_Escogido, estado FROM Participantes ORDER BY Num_Escogido ASC, estado ASC";
        String buscar = "SELECT Nombre, Apellido, Telefono, Num_Escogido, estado FROM Participantes"
                + " WHERE Num_Escogido = ? ORDER BY estado ASC";
        try {
            Connection con = Conexion.establecerConnection();
            if (Num.equalsIgnoreCase("")) {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
            } else {
                ps = con.prepareStatement(buscar);
                ps.setString(1, Num);
                rs = ps.executeQuery();
            }
            while (rs.next()) {
                Participantes parti = new Participantes();
                parti.setNombre(rs.getString("Nombre"));
                parti.setApellido(rs.getString("Apellido"));
                parti.setTelefono(rs.getString("Telefono"));
                parti.setNumEscogido(rs.getString("Num_Escogido"));
                parti.setEstado(rs.getString("estado"));
                listaParticipantes.add(parti);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
        return listaParticipantes;
    }

    public void CargarParticipantes(String encontrar) {
         
        Tables color = new Tables();
        elec.tb_Participantes.setDefaultRenderer(elec.tb_Participantes.getColumnClass(0), color);
        List<Participantes> lista = listarParticipantes(encontrar);
        modelo = (DefaultTableModel) elec.tb_Participantes.getModel();
        int[] ancho = {180, 200, 150, 140, 150};
        for (int i = 0; i < modelo.getColumnCount(); i++) {
            elec.tb_Participantes.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        Object[] ob = new Object[5];
        for (int i = 0; i < lista.size(); i++) {
            ob[0] = lista.get(i).getNombre();
            ob[1] = lista.get(i).getApellido();
            ob[2] = lista.get(i).getTelefono();
            ob[3] = lista.get(i).getNumEscogido();
            ob[4] = lista.get(i).getEstado();
            modelo.addRow(ob);
        }
        elec.tb_Participantes.setModel(modelo);
        JTableHeader header = elec.tb_Participantes.getTableHeader();
        header.setOpaque(false);
        header.setBackground(Color.blue);
        header.setForeground(Color.black);
    }

    public void CantidadMaxTabla() {
        PreparedStatement ps;
        ResultSet rs;
        String sql = "SELECT MAX(Num_Escogido) Num_EscogidoMaximo FROM Participantes ";
        String buscar = "SELECT COUNT(Num_Escogido) CantidadParticipantes FROM Participantes ";
        try {
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                elec.txt_numMax.setText("" + rs.getInt("Num_EscogidoMaximo"));
//                NumMaximo = Integer.parseInt(elec.txt_numMax.getText());
            }

            ps = con.prepareStatement(buscar);
            rs = ps.executeQuery();

            while (rs.next()) {
                elec.txt_cant.setText("" + rs.getInt("CantidadParticipantes"));
                elec.lb_numParticipantes.setText("" + rs.getInt("CantidadParticipantes"));
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
    }

    public void limpiarTable() {
        for (int i = 0; i < modelo.getRowCount(); i++) {
            modelo.removeRow(i);
            i = i - 1;
        }
    }
    
     public void cargarTablaGanador(String Nombre) {
        DefaultTableModel modeloTabla = (DefaultTableModel) elec.tb_ganadores.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {180, 200, 150, 140, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            elec.tb_ganadores.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String consulta = "SELECT Nombre, Apellido, Telefono, Num_Escogido, estado FROM Participantes WHERE estado LIKE '%"+Nombre+"%' ORDER BY estado ASC";
            if ("".equalsIgnoreCase(Nombre)) {
             
            } else {
                ps = con.prepareStatement(consulta);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
            }
            
            
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }

}
