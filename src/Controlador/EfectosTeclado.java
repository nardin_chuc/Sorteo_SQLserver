/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Sorteo.IngresarParticipantes;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import org.w3c.dom.events.MouseEvent;

/**
 *
 * @author CCNAR
 */
public class EfectosTeclado implements KeyListener, MouseListener{
    
    IngresarParticipantes parti;

    public EfectosTeclado(IngresarParticipantes parti) {
        this.parti = parti;
        parti.txt_telefono.addKeyListener(this);
         parti.jButton2.addMouseListener(this);
         parti.jButton1.addMouseListener(this);
         parti.btn_ingresar.addMouseListener(this);
         parti.btn_Nuevo.addMouseListener(this);
         parti.btn_Actualizar.addMouseListener(this);
         parti.btn_Eliminar.addMouseListener(this);
    }
    

    @Override
    public void keyTyped(KeyEvent e) {
   
    }

    @Override
    public void keyPressed(KeyEvent e) {
    
     
    }

    @Override
    public void keyReleased(KeyEvent e) {
         String telefono = parti.txt_telefono.getText();
     if(e.getSource() == parti.txt_telefono ){
         int contador = parti.txt_telefono.getText().length();
         if(contador == 10){
             parti.lb_TeleContador.setVisible(false);
         }else if(contador > 10){
             parti.lb_TeleContador.setText("Excede los digitos");
             parti.lb_TeleContador.setVisible(true);
         }else{
             parti.lb_TeleContador.setText("Numero Ingresados: "+(contador));
             parti.lb_TeleContador.setVisible(true);
         }
         
     }
    }

    @Override
    public void mouseClicked(java.awt.event.MouseEvent e) {
   
    }

    @Override
    public void mousePressed(java.awt.event.MouseEvent e) {
    
    }

    @Override
    public void mouseReleased(java.awt.event.MouseEvent e) {
   
    }

    @Override
    public void mouseEntered(java.awt.event.MouseEvent e) {
   if(e.getSource() == parti.btn_Eliminar){
         parti.btn_Eliminar.setBackground(Color.red);
     }else if(e.getSource() == parti.btn_Actualizar){
         parti.btn_Actualizar.setBackground(Color.red);
     }else if(e.getSource() == parti.btn_Nuevo){
         parti.btn_Nuevo.setBackground(Color.red);
     }else if(e.getSource() == parti.btn_ingresar){
         parti.btn_ingresar.setBackground(Color.red);
     }else if(e.getSource() == parti.jButton1){
         parti.jButton1.setBackground(Color.red);
     }else if(e.getSource() == parti.jButton2){
         parti.jButton2.setBackground(Color.red);
     }
    }

    @Override
    public void mouseExited(java.awt.event.MouseEvent e) {
    if(e.getSource() == parti.btn_Eliminar){
         parti.btn_Eliminar.setBackground(Color.white);
     }else if(e.getSource() == parti.btn_Actualizar){
         parti.btn_Actualizar.setBackground(Color.white);
     }else if(e.getSource() == parti.btn_Nuevo){
         parti.btn_Nuevo.setBackground(Color.white);
     }else if(e.getSource() == parti.btn_ingresar){
         parti.btn_ingresar.setBackground(Color.white);
     }else if(e.getSource() == parti.jButton1){
         parti.jButton1.setBackground(Color.white);
     }else if(e.getSource() == parti.jButton2){
         parti.jButton2.setBackground(Color.white);
     }
    }
    
}
