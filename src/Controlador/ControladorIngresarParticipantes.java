/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Sorteo.Conexion;
import Sorteo.IngresarParticipantes;
import Sorteo.Participantes;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControladorIngresarParticipantes implements ActionListener, MouseListener {

    IngresarParticipantes parti;
    Numeros num = new Numeros();
    Participantes par = new Participantes();

    public ControladorIngresarParticipantes(IngresarParticipantes parti, Numeros num, Participantes par) {
        this.parti = parti;
        this.num = num;
        this.par = par;
        parti.tb_Participantes.addMouseListener(this);
        cargarTabla();
        ActualizarId();
        parti.btn_ingresar.addActionListener(this);
        parti.btn_Nuevo.addActionListener(this);
        parti.btn_Actualizar.addActionListener(this);
        parti.btn_Eliminar.addActionListener(this);
       
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == parti.btn_ingresar) {
            if (parti.txt_nombre.getText().equals("") || parti.txt_apellido.getText().equals("")
                    || parti.txt_telefono.getText().equals("")
                    || parti.txt_NumEscogido.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Campos Vacios\n" + "Digite los datos", "Campos vacios", JOptionPane.ERROR_MESSAGE);
            } else {
                String nombre = parti.txt_nombre.getText();
                String apellido = parti.txt_apellido.getText();
                String telefono = parti.txt_telefono.getText();
                int id = Integer.parseInt(parti.txt_id.getText());//                String NumEscogido = parti.txt_NumEscogido.getText();
                int NumEscogido = Integer.parseInt(parti.txt_NumEscogido.getText());
                if (telefono.length() < 10) {
                    JOptionPane.showMessageDialog(null, "Digite correctamente el telefono");
                } else if (telefono.length() == 10) {
                    try {
                        par = num.encontrarNumero(NumEscogido);
                        if (par.getNumEscogido() != null) {
                            JOptionPane.showMessageDialog(null, "Se repite el numero escogido");

                        } else {
                            Connection conectar = Conexion.establecerConnection();
                            PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_IngresarParticipantes ?,?,?,?,? ");
                            ps.setString(1, nombre);
                            ps.setString(2, apellido);
                            ps.setString(3, telefono);
                            ps.setInt(4, NumEscogido);
                            ps.setInt(5, id);
                            ps.executeUpdate();
                            ActualizarNum(NumEscogido, "Inactivo");
                            cargarTabla();
                            limpiar();
                            ActualizarId();
                            JOptionPane.showMessageDialog(null, "Se Ingreso el Participante correctamente");
                        }

                    } catch (Exception er) {
                        System.err.println("Error: " + er.getMessage());
                    }
                }

            }

        } else if (e.getSource() == parti.btn_Actualizar) {
            if (parti.txt_nombre.getText().equals("") || parti.txt_apellido.getText().equals("")
                    || parti.txt_telefono.getText().equals("")
                    || parti.txt_NumEscogido.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Campos Vacios\n" + "Digite los datos", "Campos vacios", JOptionPane.ERROR_MESSAGE);
            } else {
                String nombre = parti.txt_nombre.getText();
                String apellido = parti.txt_apellido.getText();
                String telefono = parti.txt_telefono.getText();
                int NumEscogido = Integer.parseInt(parti.txt_NumEscogido.getText());
                int id = Integer.parseInt(parti.txt_id.getText());
                if (telefono.length() < 10) {
                    JOptionPane.showMessageDialog(null, "Digite correctamente el telefono");
                } else if (telefono.length() == 10) {
                    try {
                        par = num.encontrarNumero(NumEscogido);
                        if (par.getNumEscogido() != null) {
                            int res = JOptionPane.showConfirmDialog(null, "Se repite el numero escogido\n " + "¿ Desea seguir ?");

                            if (res == 0) {
                                Connection conectar = Conexion.establecerConnection();
                                PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_Actualizar ?,?,?,?,? ");
                                ps.setString(1, nombre);
                                ps.setString(2, apellido);
                                ps.setString(3, telefono);
                                ps.setInt(4, NumEscogido);
                                ps.setInt(5, id);
                                ps.executeUpdate();
                                ActualizarNum(NumEscogido,"Inactivo");
                                cargarTabla();
                                JOptionPane.showMessageDialog(null, "Se Modifico el Participante correctamente");
                            } else {
                                parti.txt_NumEscogido.requestFocus();
                            }

                        } else if(parti.txt_escogido.getText().equals(NumEscogido)){
                            
                            Connection conectar = Conexion.establecerConnection();
                            PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_Actualizar ?,?,?,?,? ");
                            ps.setString(1, nombre);
                            ps.setString(2, apellido);
                            ps.setString(3, telefono);
                            ps.setInt(4, NumEscogido);
                            ps.setInt(5, id);
                            ActualizarNum(NumEscogido, "Inactivo");
                            ps.executeUpdate();
                            cargarTabla();
                            JOptionPane.showMessageDialog(null, "Se Modifico el Participante correctamente");
                        }else if(!(parti.txt_escogido.getText().equals(NumEscogido))){
                            Connection conectar = Conexion.establecerConnection();
                            PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_Actualizar ?,?,?,?,? ");
                            ps.setString(1, nombre);
                            ps.setString(2, apellido);
                            ps.setString(3, telefono);
                            ps.setInt(4, NumEscogido);
                            ps.setInt(5, id);
                            ActualizarNum(NumEscogido,"Inactivo");
                            int otroNum = Integer.parseInt(parti.txt_escogido.getText());
                            ActualizarNum(otroNum,"Activo");
                            ps.executeUpdate();
                            cargarTabla();
                            JOptionPane.showMessageDialog(null, "Se Modifico el Participante correctamente");
                        }

                    } catch (Exception er) {
                        System.err.println("Error: " + e.toString());
                    }
                }

            }
        } else if (e.getSource() == parti.btn_Nuevo) {
            parti.btn_ingresar.setEnabled(true);
            limpiar();
            ActualizarId();

        } else if (e.getSource() == parti.btn_Eliminar) {

            if (parti.txt_nombre.getText().equals("") || parti.txt_apellido.getText().equals("")
                    || parti.txt_telefono.getText().equals("")
                    || parti.txt_NumEscogido.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Campos Vacios\n" + "Digite los datos", "Campos vacios", JOptionPane.ERROR_MESSAGE);
            } else {
                int res = JOptionPane.showConfirmDialog(null, "Eliminar Participante\n " + "¿ Desea seguir ?");

                if (res == 0) {
                    try {
                        int id = Integer.parseInt(parti.txt_id.getText());
                        int numElegido = Integer.parseInt(parti.txt_NumEscogido.getText());
                        ActualizarNum(numElegido, "Activo");
                        Connection conectar = Conexion.establecerConnection();
                        PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_Eliminar ?");
                        ps.setInt(1, id);
                        ps.executeUpdate();
                        cargarTabla();
                        JOptionPane.showMessageDialog(null, "Se Elimino el Participante correctamente");

                    } catch (Exception er) {
                        System.err.println("Error: " + e.toString());
                    }
                } else {

                }

            }

        }
    }

    public void cargarTabla() {
        DefaultTableModel modeloTabla = (DefaultTableModel) parti.tb_Participantes.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 150, 200};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            parti.tb_Participantes.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT * FROM clientes ORDER BY Num_Escogido ASC";
            ps = con.prepareStatement("EXECUTE SP_VerTodoParticipantes");
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }
    
    public void ActualizarNum(int Num, String estado) {
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps;
        try {
            ps = conectar.prepareStatement("UPDATE Num_Libres SET Estado = ? Where Num_Libre = ?");
            ps.setString(1, estado);
            ps.setInt(2, Num);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ControladorIngresarParticipantes.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void ActualizarId(){
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps;
        ResultSet rs;
        String id = "";
        try {
            ps = conectar.prepareStatement("SELECT MAX(id) as id FROM Participantes");
            rs = ps.executeQuery();
            while (rs.next()) {                
                id = ""+(rs.getInt("id")+1);
            }
                parti.txt_id.setText(id);
            
            
            rs.close();
        } catch (SQLException ex) {
            System.out.println("ERROR ID "+ex.getMessage());
        }
    }

    public void limpiar() {
        parti.txt_id.setText("");
        parti.txt_nombre.setText("");
        parti.txt_apellido.setText("");
        parti.txt_telefono.setText("");
        parti.txt_NumEscogido.setText("");
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == parti.tb_Participantes) {
            parti.btn_ingresar.setEnabled(false);
            try {
                int fila = parti.tb_Participantes.getSelectedRow();
                String NumElegido = parti.tb_Participantes.getValueAt(fila, 4).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id, Nombre, Apellido, Telefono, Num_Escogido FROM Participantes WHERE Num_Escogido = ?");
                ps.setString(1, NumElegido);
                rs = ps.executeQuery();
                while (rs.next()) {
                    parti.txt_id.setText(rs.getString("id"));
                    parti.txt_nombre.setText(rs.getString("Nombre"));
                    parti.txt_apellido.setText(rs.getString("Apellido"));
                    parti.txt_telefono.setText(rs.getString("Telefono"));
                    parti.txt_NumEscogido.setText(rs.getString("Num_Escogido"));
                    parti.txt_escogido.setText(rs.getString("Num_Escogido"));
                }
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

}
