/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Sorteo.Conexion;
import Sorteo.Configuraciones;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorConfiguraciones implements ActionListener, MouseListener{

    Configuraciones cf;

    public ControladorConfiguraciones(Configuraciones cf) {
        this.cf = cf;
        cargarTabla();
        this.cf.tb_Participantes.addMouseListener(this);
        this.cf.btn_Actualizar.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == cf.btn_Actualizar){
            Modificar();
        }
   }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == cf.tb_Participantes) {
          //  cf.btn_ingresar.setEnabled(false);
            try {
                int fila = cf.tb_Participantes.getSelectedRow();
                String NumElegido = cf.tb_Participantes.getValueAt(fila, 0).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id, Evento, CantidadGanadores FROM Configuracion WHERE id = ?");
                ps.setString(1, NumElegido);
                rs = ps.executeQuery();
                while (rs.next()) {
                    cf.txt_id.setText(""+rs.getInt("id"));
                    cf.txt_nombre.setText(rs.getString("Evento"));
                    cf.txt_apellido.setText(""+rs.getInt("CantidadGanadores"));
                }
                rs.close();
                cf.txt_nombre.setEnabled(true);
                cf.txt_apellido.setEnabled(true);
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
    
    public void cargarTabla() {
        DefaultTableModel modeloTabla = (DefaultTableModel) cf.tb_Participantes.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {80, 200, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            cf.tb_Participantes.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT * FROM Configuracion ";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }
    
    public void Modificar(){
        int id = Integer.parseInt(cf.txt_id.getText());
        int cant = Integer.parseInt(cf.txt_apellido.getText());
        String event = cf.txt_nombre.getText();
        try{
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE Configuracion SET Evento = ?, CantidadGanadores = ? WHERE id = ?");
            ps.setString(1, event);
            ps.setInt(2, cant);
            ps.setInt(3, id);
            ps.executeUpdate();
            cargarTabla();
            Limpiar();
        }catch(Exception e){
            System.out.println("error modificar "+e.getMessage());
        }
    }

    private void Limpiar() {
        cf.txt_apellido.setText("");
        cf.txt_nombre.setText("");
    }
}
