/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Sorteo.Conexion;
import Sorteo.NumFaltantes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorNumFaltantes {
    
    
        NumFaltantes numF;
        
        public ControladorNumFaltantes(NumFaltantes numF){
    
            this.numF = numF;
            cargarTabla();
        }
        
        public void cargarTabla() {
        DefaultTableModel modeloTabla = (DefaultTableModel) numF.tb_Participantes.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {1000, 1000};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            numF.tb_Participantes.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            
            String Estado = "Activo";
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT*FROM Num_Libres  WHERE Estado = ? ORDER BY Num_Libre ASC";
            ps = con.prepareStatement(sql);
            ps.setString(1, Estado);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }
        
}
        
    
    

