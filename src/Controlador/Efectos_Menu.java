/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Sorteo.Menu;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author CCNAR
 */
public class Efectos_Menu implements MouseListener {
    
    
    Menu mn;

    public Efectos_Menu(Menu mn) {
        this.mn = mn;
        mn.btn_Ingresar.addMouseListener(this);
        mn.btn_sorteo.addMouseListener(this);
        mn.btn_libre.addMouseListener(this);
        mn.BTN_Configuraciones.addMouseListener(this);
    }
    
    

    @Override
    public void mouseClicked(MouseEvent e) {
    
    }

    @Override
    public void mousePressed(MouseEvent e) {
   
    }

    @Override
    public void mouseReleased(MouseEvent e) {
   
    }

    @Override
    public void mouseEntered(MouseEvent e) {
     if(e.getSource() == mn.btn_Ingresar){
         mn.btn_Ingresar.setBackground(Color.red);
     }else if(e.getSource() == mn.btn_sorteo){
         mn.btn_sorteo.setBackground(Color.red);
     }else if(e.getSource() == mn.btn_libre){
         mn.btn_libre.setBackground(Color.red);
     }else if(e.getSource() == mn.BTN_Configuraciones){
         mn.BTN_Configuraciones.setBackground(Color.red);
     }
    }

    @Override
    public void mouseExited(MouseEvent e) {
   if(e.getSource() == mn.btn_Ingresar){
         mn.btn_Ingresar.setBackground(Color.white);
     }else if(e.getSource() == mn.btn_sorteo){
         mn.btn_sorteo.setBackground(Color.white);
     }else if(e.getSource() == mn.btn_libre){
         mn.btn_libre.setBackground(Color.white);
     }else if(e.getSource() == mn.BTN_Configuraciones){
         mn.BTN_Configuraciones.setBackground(Color.white);
     }
    }
    
}
