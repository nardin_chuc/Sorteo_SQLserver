/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Sorteo.NumFaltantes;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author CCNAR
 */
public class EfectosNumFaltantes implements MouseListener {
    
    NumFaltantes numf ;

    public EfectosNumFaltantes(NumFaltantes numf) {
        this.numf = numf;
        this.numf.btn_imprimir.addMouseListener(this);
         this.numf.jButton1.addMouseListener(this);
          this.numf.jButton2.addMouseListener(this);
    }
    
    

    @Override
    public void mouseClicked(MouseEvent e) {
    
    }

    @Override
    public void mousePressed(MouseEvent e) {
   
    }

    @Override
    public void mouseReleased(MouseEvent e) {
   
    }

    @Override
    public void mouseEntered(MouseEvent e) {
     if(e.getSource() == numf.btn_imprimir){
         numf.btn_imprimir.setBackground(Color.red);
     }else if(e.getSource() == numf.jButton1){
         numf.jButton1.setBackground(Color.red);
     }else if(e.getSource() == numf.jButton2){
         numf.jButton2.setBackground(Color.red);
     }
    }

    @Override
    public void mouseExited(MouseEvent e) {
   if(e.getSource() == numf.btn_imprimir){
         numf.btn_imprimir.setBackground(Color.white);
     }else if(e.getSource() == numf.jButton1){
         numf.jButton1.setBackground(Color.white);
     }else if(e.getSource() == numf.jButton2){
         numf.jButton2.setBackground(Color.white);
     }
    }
    
}
