/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Sorteo;

/**
 *
 * @author CCNAR
 */
public class Participantes {

    private int id;
private String Nombre;
private String NumEscogido;
private String Telefono;
private String estado;
private String Apellido;


    public Participantes(String NumEscogido) {
        this.NumEscogido = NumEscogido;
    }

    public Participantes(int id, String Nombre, String Apellido, String Telefono, String NumEscogido, String estado ) {
        this.id = id;
        this.Nombre = Nombre;
        this.NumEscogido = NumEscogido;
        this.Telefono = Telefono;
        this.estado = estado;
        this.Apellido = Apellido;
    }

   

    public Participantes() {
    }

    public String getTelefono() {
        return Telefono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    
    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    

    public String getNumEscogido() {
        return NumEscogido;
    }

    public void setNumEscogido(String NumEscogido) {
        this.NumEscogido = NumEscogido;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }


    
}
