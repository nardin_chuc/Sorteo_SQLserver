/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Hilos;

import Controlador.ControladorEleccion;
import Sorteo.Eleccion;
import static java.awt.Color.BLACK;
import static java.awt.Color.BLUE;
import static java.awt.Color.RED;
import static java.awt.Color.WHITE;
import java.awt.TextField;
import javax.swing.JTextField;

/**
 *
 * @author CCNAR
 */
public class HiloNum extends Thread {
    
    int contador;
    int[] numeros;
    JTextField[] numRecibidos;
    int aux;
    
    Eleccion elec;

    public HiloNum(Eleccion elec,int[] numeros,int contador, JTextField[] numRecibidos, int aux ) {
        this.elec = elec;
        this.aux = aux;
        this.contador=contador;
        this.numRecibidos = numRecibidos;
        this.numeros = numeros;
    }
    
    
    
    public void run(){
        imprimirNumeros(numeros, contador, numRecibidos, aux);
        
    }
   public void imprimirNumeros(int numeros[], int contador, JTextField []numRecibidos, int aux) {
        if (contador <= 19) {
            elec.jPanel_num_Aparecidos.removeAll();
            for (int i = 0; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                elec.jPanel_num_Aparecidos.add(numRecibidos[i]);
                elec.pack();
                if (aux == numeros[i]) {
                    numRecibidos[i].setBackground(BLUE);
                    numRecibidos[i].setForeground(WHITE);
                    elec.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    elec.pack();
                }
            }
        } else if (contador >= 20 && contador <= 39) {
            elec.jPanel_num_Aparecidos1.removeAll();
            for (int i = 20; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                elec.jPanel_num_Aparecidos1.add(numRecibidos[i]);
                elec.pack();
                if (aux == numeros[i]) {
                    numRecibidos[i].setBackground(RED);
                    numRecibidos[i].setForeground(WHITE);
                    elec.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    elec.pack();
                }
            }
        } else if (contador >= 40 && contador <= 59) {
            elec.jPanel_num_Aparecidos2.removeAll();
            for (int i = 40; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                elec.jPanel_num_Aparecidos2.add(numRecibidos[i]);
                elec.pack();
                if (aux == numeros[i]) {
                    numRecibidos[i].setBackground(BLUE);
                    numRecibidos[i].setForeground(WHITE);
                    elec.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    elec.pack();
                }
            }
        } else if (contador >= 60 && contador <= 79) {
            elec.jPanel_num_Aparecidos3.removeAll();
            for (int i = 60; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                elec.jPanel_num_Aparecidos3.add(numRecibidos[i]);
                elec.pack();
                if (aux == numeros[i]) {
                    numRecibidos[i].setBackground(RED);
                    numRecibidos[i].setForeground(WHITE);
                    elec.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    elec.pack();
                }
            }
        } else if (contador >= 80 && contador <= 89) {
            elec.jPanel_num_Aparecidos4.removeAll();
            for (int i = 80; i <= contador; i++) {
                numRecibidos[i] = new JTextField("                " + numeros[i]);
                elec.jPanel_num_Aparecidos4.add(numRecibidos[i]);
                elec.pack();
                if (aux == numeros[i]) {
                    numRecibidos[i].setBackground(BLUE);
                    numRecibidos[i].setForeground(WHITE);
                    elec.pack();
                } else {
                    numRecibidos[i].setBackground(WHITE);
                    numRecibidos[i].setForeground(BLACK);
                    elec.pack();
                }
            }
        }

    }
}
